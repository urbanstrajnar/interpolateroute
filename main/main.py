'''
Created on 27. apr. 2016

@author: Urban
'''
from netCDF4 import Dataset as NetCDFFile
import csv
from scipy import interpolate
import numpy as np

#===============================================================================
# This class opens and reads a netcdf file downloaded from ECWF
# that contains the desired variable/s in the list self.fields
#===============================================================================
class NcFile():
    def __init__(self, filename, variables):
        self.fileName = filename
        self.file = None
        self.variables = variables
        self.fields = [None for i in range(len(variables))]
        self.lat = None
        self.lon = None
        self.openFile()
        self.readFile()
        
    
    def openFile(self):
        self.file = NetCDFFile(self.fileName)
    
        
    def readFile(self):
        for i in range(len(self.fields)):
            self.fields[i] = self.file.variables[self.variables[i]]
        self.lonx = self.file.variables['longitude']
        self.latx = self.file.variables['latitude']
        self.timex = self.file.variables['time']
        
        #these can be put in memory as such
        #the variable fields cause memory issues
        self.lon = self.lonx[:]
        self.lat = self.latx[:]
        self.time = self.timex[:]

        
#===============================================================================
# Represent a single route with a few points, can also interpolate between points
# with a desired resolution
#===============================================================================
class Route():
    def __init__(self, coords, ind, points):
        self.lon = list()
        self.lat = list()
        
        #copy coordinates into object
        for coord in coords:
            self.lon.append(coord[1])
            self.lat.append(coord[0])
        if self.lon[0] < self.lon[-1]:
            self.lon.reverse()
            self.lat.reverse()
        #index of the route
        self.ind = ind
        
        #call the interpolating method
        self.lonIp, self.latIp = self.interpolateRoute(points)
    
    #calculate spline and interpolate the route
    #the values are reversed, because of the demands
    # for ascending values in the x dimension
    def interpolateRoute(self, steps):
        y = self.lon[:]
        x = self.lat[:]
        print('lon = {}'.format(y))
        print('lat = {}'.format(x))
        x.reverse()
        y.reverse()
        if len(x) < 4:
            tck = interpolate.splrep(x,y, s = 0, k = 2)
        else:
            tck = interpolate.splrep(x,y, s = 0)
        xNew = list(np.arange(x[0], x[-1], (x[-1]-x[0])/steps))
        yNew = list(interpolate.splev(xNew, tck, der = 0))
        xNew.reverse()
        yNew.reverse()

        return yNew, xNew

#===============================================================================
# interpolates values in the grid along the route at all time steps
#===============================================================================
class ValuesOnRoute():
    def __init__(self, lonIp,latIp, ind, fields):
        self.lonIp = lonIp
        self.latIp = latIp
        self.ind = ind
        self.fieldsIp = [list() for j in range(len(fields))]
        #print(lonIp)
        #print(latIp)
                
    def ipValues3D(self, lon, lat, fields, i):
        
        fieldIpFun = list()
        for j in range(len(fields)):
            #interpolator functions
            fieldIpFun.append(interpolate.interp2d(lon,lat, fields[j], kind = 'cubic'))
            
        #a list of lists, that will contain interpolated values along a route for a single time 
        single = [list() for j in range(len(fields))]
        
        #call the interpolator function for each variable
        for k in range(len(self.lonIp)):
            for j in range(len(single)):
                single[j].append(fieldIpFun[j](self.lonIp[k],self.latIp[k])[0])
        
        #put the interpolated values in a list of lists for each time
        for l in range(len(self.fieldsIp)):
            self.fieldsIp[l].append(single[l])

        
        
#=======================================================================
# This class handles everything csv related
#=======================================================================
class CsvManager():
    def __init__(self, path):
        self.path = path
    
    #reads the file containing route coordinates
    def readRouteFile(self, fileName):
        with open(self.path + fileName, newline ='') as csvFile:
            lines = csv.reader(csvFile, delimiter = ',', quotechar = '|')
            routes = list()
            coords = list()
            collect = 0
            for line in lines:
                #lines with 0 length cause issues
                if len(line) > 0:
                    
                    #read the values in a row
                    if collect > 0:
                        coords.append([float(line[0]), float(line[1])])
                        collect -= 1

                    #if the first cell contains 'pot' or 'Pot' then the
                    #following lines contain coordinates
                    elif 'pot' in line[0] or 'Pot' in line[0]:
                        
                        #gets the number of rows to be read
                        collect = int(line[1][8:])

                    
                #if finished reading coordinate rows, put coordinates in 
                #the list routes and start again
                if collect == 0 and len(coords) > 0:
                    routes.append(coords)
                    coords = list()
                        
        return routes
    
    
    #writes calculated data in an output csv file            
    def writeRoutes(self, fileName, vRoutes, time, variables, units):
        with open(self.path + fileName, 'w', newline = '') as csvfile:
            rowWriter = csv.writer(csvfile, delimiter = '\t', quotechar = '|', quoting = csv.QUOTE_MINIMAL)
            
            #write time values
            rowWriter.writerow(['time'])
            rowWriter.writerow(time)
            
            #iterate through all rows
            for vRoute in vRoutes:
                #write route data 
                rowWriter.writerow('')
                rowWriter.writerow(['route']+ [str(vRoute.ind+1)])
                rowWriter.writerow('')
                rowWriter.writerow(['lon'])
                rowWriter.writerow(vRoute.lonIp)
                rowWriter.writerow('')
                rowWriter.writerow(['lat'])
                rowWriter.writerow(vRoute.latIp)
                
                #write down interpolated values
                for i in range(len(vRoute.fieldsIp)):          
                    rowWriter.writerow('')
                    rowWriter.writerow([variables[i], units])
                    for u in vRoute.fieldsIp[i]:
                        rowWriter.writerow(u)
                    
                
    #reads filenames and variable names from an input file                
    def readInputParams(self, fileName, fileName1):
        with open(self.path + fileName , newline ='') as csvFile:
            lines = csv.reader(csvFile, delimiter = ';', quotechar = '|')
            getVariableNames = False
            for line in lines:
                if line[0] in fileName1:
                    units = line[1]
                    getVariableNames = True
                    continue
                if getVariableNames:
                    variableNames = line
                    break          
        return variableNames, units
                
            
                    
                
            
        
        
       


#===============================================================================
# Run the main program
#===============================================================================
def main():
    
    #path to directory containing files
    filePath = r"C:\Users\Urban\My Documents\LiClipse Workspace\interpolateRoute\res"
    
    #list of filenames to be read
    fileNames = [r"\low_cloud_cover",r"\mean_wave_direction",r"\mean_wave_period",r"\medium_cloud_cover",r"\surface_solar_radiation_downward",r"\veter_u_v"]
    csvManager = CsvManager(filePath)
    
    #get list of lists from input file
    print('reading routes')
    rawRoutes = csvManager.readRouteFile(r"\routes_v_1.csv")
    
    #number of points on each route
    points = 200
    
    

    for fileName in fileNames:
        fileName1 = fileName[1:]
        print('current file: ' + fileName1)
        
        #read file names and variables from a namelist file
        print('reading parameters')
        variableNames, units = csvManager.readInputParams(r"\variables.csv", fileName1)
        
        
        #read netcdf file
        print('reading data')
        nc = NcFile(filePath +fileName + r".nc", variableNames)
        
        #create list of interpolated routes
        print('interpolating routes')           
        routes = [Route(rawRoutes[i], i, points) for i in range(len(rawRoutes))]
        
        #create ValuesOnRoute objects for each individual route
        vRoutes = [ValuesOnRoute(routes[i].lonIp, routes[i].latIp, i, variableNames) for i in range(len(routes))]
        
        print('interpolating data to routes')
        #go through each route and every time and interpolate values    
        for j in range(len(routes)):
            for i in range(len(nc.time)):
                #display progress in %
                if int(((i+j) / (len(routes) + len(nc.time)))*100) % 10 == 0:
                    print("{0:.2f} %".format(((i+j) / (len(routes) + len(nc.time)))*100))
                 
                #the magic happens here    
                vRoutes[j].ipValues3D(nc.lon,nc.lat,[nc.fields[k][i] for k in range(len(nc.fields))],i)
        
        #create an output file    
        print('writing interpolated data')
        csvManager.writeRoutes(r'\interpolated_'+ fileName1+'.csv', vRoutes, nc.time, variableNames, units)
            
            
        print('done')
    print('finished')
    return
    


main()
        