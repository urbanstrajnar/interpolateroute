# README #


### Overview ###

* The program interpolateRoute gives interpolated values of u, v and gust wind speeds on a predetermined route across the atlantic
* version 1.0

### How to use ###
* You need a python version 3.0+
* Dependencies: Numpy1.9+mkl, scipy, netCDF4
* You need two input files: "podatki_veter.nc" which contains the data and
* "route.csv" which contains the routes' coordinates
* The output file is "interpolated.csv"
